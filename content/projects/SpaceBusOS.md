+++
title = "SpaceBusOS"
date = "2022-10-17"
tags = ["SpaceBusOS", "Projects"]
type = "page"
+++
## My personal spin on NixOS built on top of flakes.

### Features:
#### Improved secutiry:
- [x] Locked down firewall
- [ ] SELinux (WIP)
- [X] Removal of insecure filesystems
#### A foccus on desktop and workstations
- [X] BCacheFS by default
- [X] A custom kernel patchset for desktops
