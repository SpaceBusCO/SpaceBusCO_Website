+++
title = "Projects"
date = "2022-10-24"
tags = ["Projects", "SerialSword"]
type = "page"
+++

## Yet another USB serial debugger

Inspired by the likes of the JTAGULATOR, GreatFET, and more, I have set out to create my own low-cost USB serial debugging interface.
