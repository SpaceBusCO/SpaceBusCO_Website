+++
title = "SerialSword"
date = "2022-10-24"
tags = ["Projects", "SerialSword"]
type = "page"
+++

## Yet Another USB Serial Debugger

Inspired by the likes of the GreatFET, Jtagulator, and Pico Debug'n'Dump, I have set out to create my own USB serial debugger.


